package rs.ac.singidunum.android.vezbefragmenti;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the CustomInteraction
 * to handle interaction events.
 * Use the {@link FakultetiFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FakultetiFragment extends Fragment {
    private static final String DATA = "fakulteti";

    private ArrayList<String> data;
    private View mainView;
    private LinearLayout ll;

    private CustomInteraction mListener;

    public FakultetiFragment() {
        // Required empty public constructor
    }

    //Factory za kreiranje nove instance fragmenta
    public static FakultetiFragment newInstance(ArrayList<String> fakulteti
    ) {
        FakultetiFragment fragment = new FakultetiFragment();
        //Kreira bundle i ubaci u njega promenljive
        Bundle args = new Bundle();
        args.putStringArrayList(DATA, fakulteti);
        //ovako dodajemo argumente, odnosno bundle u fragment
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
           data = getArguments().getStringArrayList(DATA); //dobavimo vrednosti
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fakulteti, container, false);
        mainView = view;
        drawData();
        //vracamo view koji se koristi za fragment
        return view;
    }

    public void manipulisiFragmentom() {
        System.out.println("uspeh");
    }

    private void drawData(){
        ll =(LinearLayout) mainView.findViewById(R.id.fakulteti_view);
        for(int i = 0; i < data.size(); i++){
            TextView tv = new TextView(getActivity());
            tv.setText(data.get(i));
            tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onItemClicked(view); //prosledi event main activity-u
                }
            });
            ll.addView(tv);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        //ova metoda se poziva kada se nakaci na context, odnosno na activity i desi se sledece
        //proveravamo da li activity implementira nas interfejs
        if (context instanceof CustomInteraction) {
            //ako implementira, dobijamo referencu na activity
            mListener = (CustomInteraction) context;
        } else {
            //u suportnom bacamo gresku, jer nije moguce komunicirati
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
