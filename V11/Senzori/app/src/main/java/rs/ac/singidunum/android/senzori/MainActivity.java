package rs.ac.singidunum.android.senzori;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements LocationListener {
    TextView label;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initComponents();
        initGps();
    }

    private void initComponents(){
        label = findViewById(R.id.label);
    }

    private void initGps() {
        LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            System.out.println("No Permission");
            String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION};
            ActivityCompat.requestPermissions((Activity) this, permissions, 0);
            //return;
        }

        //potreban nam je GPS provider, navodimo minimalno vreme za update, kao i minimalnu udaljenost za update
        //konacno navodimo i LocationListener objekat koji treba da reaguje (u nasem slucaju nas main activity)
        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10, this); //fine location
        //lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER); //coarse location


    }

    //metoda koja se poziva nakon promene lokacije
    @Override
    public void onLocationChanged(Location location) {
        double lat = location.getLatitude();
        double lon = location.getLongitude();
        label.setText("lat:" + lat + "\n lon:"+lon);

    }

    //metoda koja se poziva nakon sto se promeni status provider-a lokacije, na primer kada
    //provider nije u stanju da dobavi neku lokaciju, ili je postao dostupan nakon nekog perioda
    //nedostupnosti
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    //metoda koja se poziva kada je provider lokacije osposobljen, odnosno ukljucen od strane korisnika
    @Override
    public void onProviderEnabled(String provider) {

    }


    //metoda koja se poziva kada je provider lokacije onesposobljen, odnosno iskljucen od strane korisnika
    @Override
    public void onProviderDisabled(String provider) {

    }
}
