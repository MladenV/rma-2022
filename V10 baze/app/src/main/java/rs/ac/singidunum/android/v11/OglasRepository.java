package rs.ac.singidunum.android.v11;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class OglasRepository {

    private Database database;

    public OglasRepository(Database db){
        this.database = db;
    }

    public void addOglas(String naslov, double cena){
        SQLiteDatabase db = database.getWritableDatabase();
        //da bismo uneli podatke u bazu, treba nam skup vrednosti, ContentValues
        ContentValues cv = new ContentValues();

        cv.put(Oglas.FIELD_NASLOV, naslov);
        cv.put(Oglas.FIELD_CENA, cena);
        //radimo insert ovog skupa podataka
        db.insert(Oglas.TABLE_NAME, null, cv);
    }

    public void editOglas(int oglasId, String naslov, double cena){
        SQLiteDatabase db = database.getWritableDatabase();
        //da bismo uneli podatke u bazu, treba nam skup vrednosti, ContentValues
        ContentValues cv = new ContentValues();

        cv.put(Oglas.FIELD_NASLOV, naslov);
        cv.put(Oglas.FIELD_CENA, cena);
        //radimo update baze, prosledjujemo tabelu, skup podataka, diskriminator u formi prep. statementa za pronalazenje torke
        //i konacno kao niz argumenata vrednosti koje se ubacuju u prep.statement
        db.update(Oglas.TABLE_NAME, cv, Oglas.FIELD_OGLAS_ID + "=?", new String[] {String.valueOf(oglasId)});
    }

    public int deleteOglas(int oglasId){
        int numDeleted = 0;

        SQLiteDatabase db = database.getWritableDatabase();
        numDeleted = db.delete(Oglas.FIELD_CENA, Oglas.FIELD_OGLAS_ID + "=?", new String[] {String.valueOf(oglasId)});
        return numDeleted;
    }

    public Oglas getOglasbyId(int oglasId){
        SQLiteDatabase db = database.getReadableDatabase();
        // SELECT * FROM oglas WHERE oglas_id = ?
        String query = String.format("SELECT * FROM %s WHERE %s = ?", Oglas.TABLE_NAME, Oglas.FIELD_OGLAS_ID);
        Cursor result = db.rawQuery(query, new String[] {String.valueOf(oglasId)});
        if(result.moveToFirst()) { //ako ima
            String naslov = result.getString(result.getColumnIndex(Oglas.FIELD_NASLOV)); //prima columnIndex odnosno redni broj kolone
            Double cena = result.getDouble(result.getColumnIndex(Oglas.FIELD_CENA));
            return new Oglas(oglasId, naslov, cena);
        } else{
            return null;
        }
    }

    public List<Oglas> getAllOglasi(){
        SQLiteDatabase db = database.getReadableDatabase();
        String query = String.format("SELECT * FROM %s", Oglas.TABLE_NAME);
        Cursor result = db.rawQuery(query, null);
        result.moveToFirst();
        List<Oglas> list = new ArrayList<Oglas>(result.getCount()); //kreiramo listu sa onoliko elemenata koliko je nadjeno
        while(!result.isAfterLast()){ //dok nismo izasli posle poslednjeg
            int oglasId = result.getInt(result.getColumnIndex(Oglas.FIELD_OGLAS_ID));
            String naslov = result.getString(result.getColumnIndex(Oglas.FIELD_NASLOV));
            double cena= result.getDouble(result.getColumnIndex(Oglas.FIELD_CENA));

            //kreiramo objekat i dodajemo u listu
            list.add(new Oglas(oglasId, naslov, cena));
            //pomeramo kursor
            result.moveToNext();
        }
        return list;
    }
}
