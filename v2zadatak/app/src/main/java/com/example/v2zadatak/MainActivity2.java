package com.example.v2zadatak;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Context ctx = this;

        String korIme = getIntent().getStringExtra("korIme");
        String lozinka = getIntent().getStringExtra("lozinka");

        findViewById(R.id.dugmeDalje).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ctx, MainActivity3.class);
                i.putExtra("korIme", korIme);
                i.putExtra("lozinka",lozinka);
                i.putExtra("ime", ((EditText) findViewById(R.id.inputIme)).getText().toString());
                i.putExtra("prezime", ((EditText) findViewById(R.id.inputPrezime)).getText().toString());
                i.putExtra("email", ((EditText) findViewById(R.id.inputEmail)).getText().toString());
                startActivity(i);
                finish();
            }
        });

        findViewById(R.id.dugmeOdustani2).setOnClickListener(view -> {
            finish();
        });

    }
}