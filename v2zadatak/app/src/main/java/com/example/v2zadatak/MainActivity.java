package com.example.v2zadatak;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button dugmeRegistrujSe = findViewById(R.id.dugmeRegistracija);
        Intent i = new Intent(this, MainActivity2.class);
        dugmeRegistrujSe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i.putExtra("korIme", ((EditText) findViewById(R.id.inputKorIme)).getText().toString());
                i.putExtra("lozinka",((EditText) findViewById(R.id.inputLozinka)).getText().toString());
                startActivity(i);

            }
        });

        findViewById(R.id.dugmeOdustani).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
}