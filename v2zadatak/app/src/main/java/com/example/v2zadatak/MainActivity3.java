package com.example.v2zadatak;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        String korIme = getIntent().getStringExtra("korIme");
        String lozinka = getIntent().getStringExtra("lozinka");
        String ime = getIntent().getStringExtra("ime");
        String prezime = getIntent().getStringExtra("prezime");
        String email = getIntent().getStringExtra("email");

        ((TextView)findViewById(R.id.verKorIme)).setText(korIme);
        ((TextView)findViewById(R.id.verIme)).setText(ime);
        ((TextView)findViewById(R.id.verPrezime)).setText(prezime);
        ((TextView)findViewById(R.id.verEmail)).setText(email);

        findViewById(R.id.odustani3).setOnClickListener(view -> {finish();});
        findViewById(R.id.potvrdi3).setOnClickListener(view -> {
            if(((EditText)findViewById(R.id.verLozinka)).getText().toString().equals(lozinka)){
                Toast.makeText(this, "Registracija uspešna", Toast.LENGTH_LONG).show();
            }
            //poslednji activity ne zahteva nikakve podatke u intentu, samo se prelazi na activity
            //u kojem piše da je registracija uspešna
        });


    }
}