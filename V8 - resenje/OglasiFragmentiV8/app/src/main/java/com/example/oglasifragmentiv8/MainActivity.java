package com.example.oglasifragmentiv8;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    private SviOglasiFragment svi;
    private JedanOglasFragment jedan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        OglasViewModel ovm = new ViewModelProvider(this).get(OglasViewModel.class);
        ovm.dobaviOdabranOglas().observe(this, odabranOglas ->{
            prebaciFragment();
        });

        //postavljanje početnog fragmenta
        svi = SviOglasiFragment.newInstance();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.fragmentcontainer, svi, null);
        ft.commit();
    }


    private void prebaciFragment(){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.remove(svi);
        ft.add(R.id.fragmentcontainer, JedanOglasFragment.newInstance(), null);
        ft.addToBackStack(null);
        ft.commit();
    }



}