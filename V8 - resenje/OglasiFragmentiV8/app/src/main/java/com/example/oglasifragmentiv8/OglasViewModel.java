package com.example.oglasifragmentiv8;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OglasViewModel extends ViewModel {
    private MutableLiveData<List<Oglas>> sviOglasi;
    private MutableLiveData<Oglas> odabraniOglas = new MutableLiveData<>();

    public LiveData<List<Oglas>> dobaviOglase(){
        if(sviOglasi == null){
            sviOglasi = new MutableLiveData<>();
            generisiOglase();
        }

        return sviOglasi;
    }

    private void generisiOglase(){
        Oglas o1 = new Oglas(1, "Naslov oglasa 1", "Adresa oglasa 1", 150000, "Opis prvog oglasa");
        Oglas o2 = new Oglas(2, "Naslov oglasa 2", "Adresa oglasa 2", 150000, "Opis drugog oglasa");
        Oglas o3 = new Oglas(3, "Naslov oglasa 3", "Adresa oglasa 3", 150000, "Opis treceg oglasa");

        List<Oglas> podaci = new ArrayList<>(Arrays.asList(o1, o2, o3));
        sviOglasi.setValue(podaci);
    }

    public LiveData<Oglas> dobaviOdabranOglas(){
        return odabraniOglas;
    }

    public void odaberiOglas(int id){
        for(Oglas o : sviOglasi.getValue()){
            if(o.getId() == id){
                odabraniOglas.setValue(o);
            }
        }
    }

}
