package com.example.oglasifragmentiv8;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;


public class JedanOglasFragment extends Fragment {

    private OglasViewModel ovm;

    public JedanOglasFragment() {
        // Required empty public constructor
    }

    public static JedanOglasFragment newInstance() {
        JedanOglasFragment fragment = new JedanOglasFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_jedan_oglas, container, false);
        ovm = new ViewModelProvider(requireActivity()).get(OglasViewModel.class);
        //ista stvar kao u drugom fragmentu, samo bez lambde
        ovm.dobaviOdabranOglas().observe(getViewLifecycleOwner(), new Observer<Oglas>() {
            @Override
            public void onChanged(Oglas oglas) {
                prikaziOglas(v, oglas);
            }
        });
        return v;


    }

    private void prikaziOglas(View v, Oglas o){
        ((EditText)v.findViewById(R.id.inputNaslov)).setText(o.getNaslov());
        ((EditText)v.findViewById(R.id.inputAdresa)).setText(o.getAdresa());
        ((EditText)v.findViewById(R.id.inputCena)).setText(String.valueOf(o.getCena()));
        ((EditText)v.findViewById(R.id.inputOpis)).setText(o.getOpis());
    }
}