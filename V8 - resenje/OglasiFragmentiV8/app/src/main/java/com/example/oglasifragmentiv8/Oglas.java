package com.example.oglasifragmentiv8;

public class Oglas {

    private int id;
    private String naslov;
    private String adresa;
    private float cena;
    private String opis;

    public Oglas(){

    }

    public Oglas(int id, String naslov, String adresa, float cena, String opis){
        this.id = id;
        this.naslov = naslov;
        this.adresa = adresa;
        this.cena = cena;
        this.opis = opis;
    }

    public int getId(){
        return id;
    }

    public String getNaslov(){
        return naslov;
    }

    public String getAdresa(){
        return adresa;
    }

    public float getCena(){
        return cena;
    }

    public String getOpis(){
        return opis;
    }


}
