package com.example.oglasifragmentiv8;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SviOglasiFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SviOglasiFragment extends Fragment {
    private OglasViewModel ovm;

    public SviOglasiFragment() {
        // Required empty public constructor
    }

    public static SviOglasiFragment newInstance() {
        SviOglasiFragment fragment = new SviOglasiFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_svi_oglasi, container, false);
        ovm = new ViewModelProvider(requireActivity()).get(OglasViewModel.class);
        LinearLayout oglasiContainer = (LinearLayout) v.findViewById(R.id.oglasicontainer);
        ovm.dobaviOglase().observe(getViewLifecycleOwner(), sviOglasi ->{
            prikaziOglase(oglasiContainer, sviOglasi);
        });
        return v;
    }

    private void prikaziOglase(ViewGroup container, List<Oglas> sviOglasi){
        for(Oglas o : sviOglasi){
            TextView t = new TextView(requireActivity());
            t.setText(o.getNaslov());
            t.setTag(o.getId());
            t.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ovm.odaberiOglas((int) view.getTag());
                }
            });
            container.addView(t);
        }
    }
}