package rs.ac.singidunum.android.vezbe3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.zip.Inflater;

public class MainActivity extends AppCompatActivity {
    Context c;
    ArrayList<Kontakt> kontakti;
    ArrayList<Kontakt> shownKontakti;
    LinearLayout mainLayout;
    LinearLayout subLayout;
    ArrayList<LinearLayout> children;
    EditText search;
    Button addButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        c = this;
        children = new ArrayList<>();
        kontakti = (ArrayList<Kontakt>)getIntent().getSerializableExtra("kontakti");
        if(kontakti == null)
            setupData();
        generateData();
        setupSearch();
        setupButton();

    }
    private void setupButton(){
        addButton = findViewById(R.id.addButton);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, EditKontaktActivity.class);
                i.putExtra("kontakti", kontakti);
                startActivity(i);
                finish();
            }
        });
    }

    private void setupSearch(){
        search = findViewById(R.id.inputSearch);
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                shownKontakti = new ArrayList<Kontakt>();
                if(search.getText().equals("")){
                    shownKontakti = kontakti;
                }
                else{
                    for(Kontakt k : kontakti){
                        if(k.getIme().contains(search.getText().toString())||
                                k.getPrezime().contains(search.getText().toString()) ||
                                k.getSkypeId().contains(search.getText().toString())){
                            shownKontakti.add(k);
                        }
                    }
                    clearAndGenerate();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void setupData(){
        kontakti = new ArrayList<Kontakt>();
        for(int i = 0; i < 10; i++){ //dodamo 10 kontakata
            kontakti.add(new Kontakt("Ime" + i, "Prezime" + i,
                    "tel" + i, "skype" + i));
        }
    }
    private void clearAndGenerate(){

        for(Kontakt k : kontakti){
            if(!shownKontakti.contains(k)){
                children.get(kontakti.indexOf(k)).setVisibility(View.GONE);
            }
            else{
                children.get(kontakti.indexOf(k)).setVisibility(View.VISIBLE);
            }
        }
    }

    //Prvobitno generisanje sadrzaja, ima dosta dupliranja koda
    private void generateData(){
        LayoutInflater inflater = LayoutInflater.from(this);
        mainLayout = findViewById(R.id.mainLayout);

        LinearLayout kontaktContent;
        Button delButton;
        TextView ime, prezime, tel, skype;
        ImageView slika;

        boolean color = false;
        for(final Kontakt k : kontakti){
            subLayout = (LinearLayout) inflater.inflate(R.layout.kontaktlayout,
                    mainLayout, false);
            children.add(subLayout);
            //tempChild = subLayout;
            //podesavamo podatke za svaki kontakt

            ime = subLayout.findViewById(R.id.labelIme);
            prezime = subLayout.findViewById(R.id.labelPrezime);
            tel = subLayout.findViewById(R.id.labelTel);
            skype = subLayout.findViewById(R.id.labelSkype);
            slika = subLayout.findViewById(R.id.imageView);
            delButton = subLayout.findViewById(R.id.delButton);
            kontaktContent = subLayout.findViewById(R.id.kontaktContent);

            kontaktContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(MainActivity.this, EditKontaktActivity.class);
                    i.putExtra("indeks", kontakti.indexOf(k));
                    i.putExtra("kontakti", kontakti);
                    startActivity(i);
                    finish();
                }
            });

            delButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    mainLayout.removeView(children.get(kontakti.indexOf(k)));
                    children.remove(kontakti.indexOf(k));
                    kontakti.remove(k);
                }
            });

            ime.setText(k.getIme());
            prezime.setText(k.getPrezime());
            tel.setText(k.getBrojTelefona());
            skype.setText(k.getSkypeId());
            slika.setImageResource(R.drawable.cat);

            if(!color){
                color = true;
                subLayout.setBackgroundColor(Color.CYAN);
            }
            else{
                color = false;
                subLayout.setBackgroundColor(Color.GRAY);
            }
            mainLayout.addView(subLayout);
        }

    }
}
