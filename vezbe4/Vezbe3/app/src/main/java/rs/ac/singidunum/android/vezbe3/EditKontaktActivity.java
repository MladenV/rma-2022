package rs.ac.singidunum.android.vezbe3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

public class EditKontaktActivity extends AppCompatActivity {

    private int indeks;
    private Kontakt k;
    private ArrayList<Kontakt> kontakti;
    EditText ime, prezime, brTel, skype;
    Button confirm, forfeit;
    Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_kontakt);
        kontakti = (ArrayList<Kontakt>)getIntent().getSerializableExtra("kontakti");
        indeks = getIntent().getIntExtra("indeks", -1);
        if(indeks == -1){
            addKontakt();
        }
        else{
            editKontakt();
        }
    }

    public void addKontakt(){
        //dupliranje koda
        ime = findViewById(R.id.editName);
        prezime = findViewById(R.id.editSurname);
        brTel = findViewById(R.id.editPhone);
        skype = findViewById(R.id.editSkype);
        confirm = findViewById(R.id.confirmButton);
        forfeit = findViewById(R.id.forfeitButton);


        k = new Kontakt();
        i = new Intent(EditKontaktActivity.this, MainActivity.class);
        forfeit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                i.putExtra("kontakti", kontakti);
                startActivity(i);
                finish();
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                k.setIme(ime.getText().toString());
                k.setPrezime(prezime.getText().toString());
                k.setBrojTelefona(brTel.getText().toString());
                k.setSkypeId(skype.getText().toString());
                kontakti.add(k);
                i.putExtra("kontakti", kontakti);
                startActivity(i);
                finish();

            }
        });
    }

    public void editKontakt(){
        ime = findViewById(R.id.editName);
        prezime = findViewById(R.id.editSurname);
        brTel = findViewById(R.id.editPhone);
        skype = findViewById(R.id.editSkype);
        confirm = findViewById(R.id.confirmButton);
        forfeit = findViewById(R.id.forfeitButton);

        k = kontakti.get(indeks);

        ime.setText(k.getIme());
        prezime.setText(k.getPrezime());
        brTel.setText(k.getBrojTelefona());
        skype.setText(k.getSkypeId());
        i = new Intent(EditKontaktActivity.this, MainActivity.class);
        forfeit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                i.putExtra("kontakti", kontakti);
                startActivity(i);
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                kontakti.get(indeks).setIme(ime.getText().toString());
                kontakti.get(indeks).setPrezime(prezime.getText().toString());
                kontakti.get(indeks).setBrojTelefona(brTel.getText().toString());
                kontakti.get(indeks).setSkypeId(skype.getText().toString());
                i.putExtra("kontakti", kontakti);
                startActivity(i);

            }
        });



    }
}
