package rs.ac.singidunum.android.vezbe3;

import java.io.Serializable;

public class Kontakt implements Serializable {
    private String ime;
    private String prezime;
    private String brojTelefona;
    private String skypeId;

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getBrojTelefona() {
        return brojTelefona;
    }

    public void setBrojTelefona(String brojTelefona) {
        this.brojTelefona = brojTelefona;
    }

    public String getSkypeId() {
        return skypeId;
    }

    public void setSkypeId(String skypeId) {
        this.skypeId = skypeId;
    }

    public Kontakt(){

    }

    public Kontakt(String ime, String prezime, String brTel, String skype){
        this.ime = ime;
        this.prezime = prezime;
        this.brojTelefona = brTel;
        this.skypeId = skype;
    }
}
