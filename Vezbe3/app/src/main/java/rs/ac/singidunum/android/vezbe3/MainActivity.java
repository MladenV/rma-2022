package rs.ac.singidunum.android.vezbe3;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.zip.Inflater;

public class MainActivity extends AppCompatActivity {

    ArrayList<Kontakt> kontakti;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupData();
        generateData();

    }

    private void setupData(){
        kontakti = new ArrayList<Kontakt>();
        for(int i = 0; i < 10; i++){ //dodamo 10 kontakata
            kontakti.add(new Kontakt("Ime" + i, "Prezime" + i,
                    "tel" + i, "skype" + i));
        }
    }

    private void generateData(){
        LayoutInflater inflater = LayoutInflater.from(this);
        LinearLayout mainLayout = findViewById(R.id.mainLayout);
        LinearLayout subLayout;
        TextView ime, prezime, tel, skype;
        ImageView slika;
        boolean color = false;
        for(Kontakt k : kontakti){
            subLayout = (LinearLayout) inflater.inflate(R.layout.kontaktlayout,
                    mainLayout, false);

            //podesavamo podatke za svaki kontakt

            ime = subLayout.findViewById(R.id.labelIme);
            prezime = subLayout.findViewById(R.id.labelPrezime);
            tel = subLayout.findViewById(R.id.labelTel);
            skype = subLayout.findViewById(R.id.labelSkype);
            slika = subLayout.findViewById(R.id.imageView);

            ime.setText(k.getIme());
            prezime.setText(k.getPrezime());
            tel.setText(k.getBrojTelefona());
            skype.setText(k.getSkypeId());
            slika.setImageResource(R.drawable.cat);
            if(!color){
                color = true;
                subLayout.setBackgroundColor(Color.CYAN);
            }
            else{
                color = false;
                subLayout.setBackgroundColor(Color.GRAY);
            }
            mainLayout.addView(subLayout);
        }

    }
}
