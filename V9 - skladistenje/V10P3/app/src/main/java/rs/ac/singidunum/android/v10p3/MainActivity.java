package rs.ac.singidunum.android.v10p3;

import android.Manifest;
import android.app.Activity;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class MainActivity extends AppCompatActivity {

    private final static String DATA_PREFIX = "MainActivityDataDir";
    private final static String DATA_FILE = "/podaci-korisnika.txt";
    private final static String DATA_IME = "ime";
    private final static String DATA_EMAIL = "email";

    private EditText inputIme;
    private EditText inputEmail;
    private Button buttonSave;
    private Button buttonLoad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    private void init(){
        inputIme = (EditText) findViewById(R.id.inputIme);
        inputEmail = (EditText) findViewById(R.id.inputEmail);

        buttonSave = (Button) findViewById(R.id.buttonSave);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveData();
            }
        });

        buttonLoad = (Button) findViewById(R.id.buttonLoad);
        buttonLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 0:
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    //Granted.
                }
                else{
                    //Denied.
                }
                break;
        }
    }

    private void saveData(){
        String ime = inputIme.getText().toString();
        String email = inputEmail.getText().toString();
        File dir = null;
        System.out.println(Environment.getExternalStorageState());
        if(Build.VERSION.SDK_INT >= 19){
            String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
            ActivityCompat.requestPermissions((Activity) this, permissions, 0);
            dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS); //string konstanta

        }else{
            dir = new File(Environment.getExternalStorageDirectory() + "/Downloads");
        }

        dir.mkdirs();

        File f = new File(dir, DATA_FILE);
        try {
            PrintWriter pw = new PrintWriter(f);
            pw.println(ime);
            pw.println(email);
            pw.flush();
            pw.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void loadData(){
        try {

            File dir = null;
            if(Build.VERSION.SDK_INT >= 19){
                dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            }else{
                dir = new File(Environment.getExternalStorageDirectory() + "/Downloads");
            }
            File f = new File(dir.getAbsolutePath() + DATA_FILE);

            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
            String ime = br.readLine();
            String email = br.readLine();
            br.close();

            inputIme.setText(ime);
            inputEmail.setText(email);

        }catch(Exception e){
            e.printStackTrace();
        }

    }

}