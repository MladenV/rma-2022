package rs.ac.singidunum.android.vezbeasync;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ProgressBar pb, pb2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pb = findViewById(R.id.progressBar);
        pb2 = findViewById(R.id.progressBar2);
        initRealEstate();
        //pb.setVisibility(View.GONE);
        //pb2.setVisibility(View.GONE);

        //initRealEstate2();
        //initRealEstate3();
    }

    @SuppressLint("HandlerLeak")
    public void initRealEstate(){
        ApiHandler.getJSON("http://192.168.91.122:5000/json", pb, pb2, new ReadDataHandler(){
            @Override
            public void handleMessage(Message msg){ //napomena: mogli smo ovo definisati unutar naše klase, da počistimo kod
                String odgovor = getJson();
                RealEstate r;
                ArrayList<RealEstate> res;
                System.out.println(odgovor);
                try{
                    //JSONObject o = new JSONObject(odgovor);
                    //r = RealEstate.fromJson(o);
                    //r = RealEstate.convertJson(odgovor);
                    //System.out.println(r.getArea());
                    //System.out.println(r.getArea());

                    TextView label1 = findViewById(R.id.label1);
                    label1.setText(odgovor);
                }
                catch(Exception e){
                    e.printStackTrace();
                }

            }

        });
    }

    public void initRealEstate2(){
        TaskExecutor taskExecutor = new TaskExecutor();
        TextView label1 = findViewById(R.id.label1);
        taskExecutor.executeAsync(new ConcreteTask("http://192.168.0.12:5000/json"), label1);
    }

    public void initRealEstate3() {
        TextView label1 = findViewById(R.id.label1);

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        String url = "http://192.168.0.12:5000/json";
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                System.out.println(response);
                label1.setText(response);


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error);
            }
        });

        queue.add(request);
    }

}
