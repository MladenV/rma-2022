package rs.ac.singidunum.android.vezbeasync;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.Callable;

public class ConcreteTask implements Callable<String> {

    String urlParam;

    public ConcreteTask(){
        this.urlParam ="";
    }

    public ConcreteTask(String url){
        this.urlParam = url;
    }

    @Override
    public String call() throws Exception { 
        String response = "";

        //dobavljanje konekcije
        try{
            URL url = new URL(urlParam); //prvi parametar
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String red;
            int i = 0;
            while((red=br.readLine()) != null){
                i+=1;
                response += red;
                Thread.sleep(2000);
                //publishProgress(i);
            }
            br.close();
            con.disconnect();

        }catch(Exception e){
            e.printStackTrace();
        }

        return response;
    }
}
