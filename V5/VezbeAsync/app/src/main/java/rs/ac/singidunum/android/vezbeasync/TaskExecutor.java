package rs.ac.singidunum.android.vezbeasync;

import android.os.Handler;
import android.os.Looper;
import android.widget.TextView;

import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class TaskExecutor {

    private final Executor executor = Executors.newSingleThreadExecutor();
    private final Handler handler = new Handler(Looper.getMainLooper());


    public interface Callback<R>{
        void onComplete(R result);
    }

    public void executeAsync(Callable<String> callable, TextView tv){
        executor.execute(new Runnable() {
            @Override
            public void run() {

                try {
                    //pozivanje konkretnog taska
                    final String result = callable.call();

                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            //callback.onComplete(result)
                            tv.setText(result);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

}